package com.example.trevor.simplecameraapp;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomAdapter extends BaseAdapter{

    Context context;
    ArrayList<HashMap<String,String>> result;
    ArrayList<Bitmap> images;
    private static LayoutInflater inflater=null;
    public CustomAdapter(PhoneActivity mainActivity, ArrayList<HashMap<String,String>> map, ArrayList<Bitmap> bitmaps) {
        // TODO Auto-generated constructor stub
        result=map;
        context=mainActivity;
        images = bitmaps;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        TextView tv2;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        HashMap<String,String> data = new HashMap<String,String>();
        Bitmap image;
        data = result.get(position);
        image = images.get(position);
        rowView=convertView;
        if(convertView==null)
            rowView = inflater.inflate(R.layout.mylist, null);
        holder.tv=(TextView) rowView.findViewById(R.id.firstLine);
        holder.tv2=(TextView) rowView.findViewById(R.id.secondLine);
        holder.img=(ImageView) rowView.findViewById(R.id.icon);
        holder.tv.setText(data.get(PhoneActivity.DESC_KEY));
        holder.tv2.setText(data.get(PhoneActivity.PRICE_KEY));
        try {
            holder.img.setImageBitmap(Bitmap.createScaledBitmap(image, 120, 120, false));
        }
        catch (NumberFormatException e)
        {
            Log.e("asdf",e.getMessage());
        }
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+result.get(position).get(PhoneActivity.DESC_KEY), Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}